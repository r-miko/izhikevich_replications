# Replicating the Behaviour of Izhikevich Point Neurons

This repo provides a single python script that uses Izhikevich point neurons to produce common neuronal behaviours. 

### Reference

- **E. M. Izhikevich** (2003). Simple model of spiking neurons. *IEEE Transactions on Neural Networks* vol. 14, no. 6, pp. 1569-1572, DOI: [10.1109/TNN.2003.820440](https://doi.org/10.1109/TNN.2003.820440).

- **E. M. Izhikevich** (2007). Dynamical systems in neuroscience. *MIT press*.

